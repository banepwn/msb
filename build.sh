#!/bin/bash
# Usage: ./build.sh <repo path> <config name>
# Example: ./build.sh ~/yourgame-repo release
# You are expected to manually edit this file for your game.
set -e
tmpdir="/tmp/msb"
pushd "$(dirname -- "$(realpath -- "$0")")" # https://stackoverflow.com/a/40700120

outpath="$(./preprocess.lua -p $1 -c configs/$2.json builds)"
mkdir -p "$outpath/music"
for file in "$1/music/"*.mmpz
do
	lmms render "$file" -l -f ogg -o "$outpath/music/$(basename "$file" .mmpz).ogg" # https://stackoverflow.com/a/12152997
done

mkdir -p "$tmpdir"
mv "$outpath/"* "$tmpdir"
./packers/windows.sh "love/11.3/windows" "yourgame" "$tmpdir" "$outpath/pkg-win64.zip" "override/win64/love.ico"
./packers/linux64ai.sh "love/11.3/linux64ai" "override/linux64ai" "$tmpdir" "$outpath/pkg-lin64.AppImage"
rm -r "$tmpdir"

echo -e "\n$outpath"
popd
