#!/bin/bash
# Usage: ./packers/linux_ai.sh <LÖVE path> <override path> <input dir> <output dir>
set -e
tmp="/tmp/packer"
rm -rf "$tmp"
mkdir -p "$tmp/a"

cp -r "$1/"* "$tmp/a"
cp -r "$2/"* "$tmp/a"
rm -f "$tmp/a/.gitkeep"

pushd "$3"
zip -9 -r "$tmp/b.zip" .
popd

cat "$tmp/a/usr/bin/love" "$tmp/b.zip" > "$tmp/c"
rm "$tmp/a/usr/bin/love"
mv "$tmp/c" "$tmp/a/usr/bin/love"
chmod +x "$tmp/a/usr/bin/love"

appimagetool "$tmp/a" "$tmp/d.AppImage"

mv "$tmp/d.AppImage" "$4"
rm -r "$tmp"
