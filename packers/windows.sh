#!/bin/bash
# Usage: ./packers/windows.sh <LÖVE path> <exe name> <input dir> <output dir> [icon path]
set -e
tmp="/tmp/packer"
rm -rf "$tmp"
mkdir -p "$tmp/a"

cp "$1/"* "$tmp/a"
rm "$tmp/a/changes.txt"
rm "$tmp/a/game.ico"
rm -f "$tmp/a/.gitkeep"
#rm "$tmp/a/license.txt"
rm "$tmp/a/love.ico"
rm "$tmp/a/lovec.exe"
rm "$tmp/a/readme.txt"

echo "$3"
pushd "$3"
zip -9 -r "$tmp/b.zip" .
popd
cat "$tmp/a/love.exe" "$tmp/b.zip" > "$tmp/c.exe"
rm "$tmp/a/love.exe"
if [ ! -z "$5" ]
then
	./iconchange.lua "$tmp/c.exe" "$5" "$tmp/a/$2.exe"
else
	mv "$tmp/c.exe" "$tmp/a/$2.exe"
fi
pushd "$tmp/a"
zip -9 -r "$tmp/d.zip" .
popd

mv "$tmp/d.zip" "$4"
rm -r "$tmp"
