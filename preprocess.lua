#!/usr/bin/luajit
local argparse = require "argparse"
local parser = argparse({
	name = "preprocess.lua",
	description = "A preprocessor for LÖVE/LÖVR projects."
})
parser:argument("input", "Input directory.")
parser:argument("output", "Output parent directory.")
parser:option("-c --config", "Configuration file.", "configs/release.json")
parser:option("-j --compiler", "Command to use for LuaJIT. This must match the version used in the engine or it will not run.", "luajit")
parser:flag("-v --verbose", "Enable verbose output.")
parser:flag("-p --shell", "Only output the output directory name.")

local args = parser:parse()

local log = args.shell and function() end or function(...) print(string.format(...)) end
local logv = args.verbose and log or function() end
local logs = args.shell and function(str) io.stdout:write(str) end or function() end
local function exec(...)
	local handle = io.popen(string.format(...))
	local result = handle:read("*a")
	handle:close()
	return result
end

args.input = args.input:gsub("/$", "").."/./"

local posix = require "posix"
assert((posix.stat(args.input ) or {}).type == "directory", "input must be a directory")
assert((posix.stat(args.output) or {}).type == "directory", "output must be a directory")
assert((posix.stat(args.config) or {}).type == "regular"  , "config must exist and be a file")

local cjson = require "cjson"
local f = assert(io.open(args.config, "r"), "failed to open config")
local c = f:read("*a")
f:close()
c = assert(cjson.decode(c), "failed to decode config")

exec("luajit -v")

local time = os.time()
local date1 = os.date("!%Y%m%d-%H%M%S", time)
local date2 = os.date("!%Y-%m-%d %H:%M:%S", time)
log("Build time: %s", date2)

local outpath = string.format("%s/build-%s-%s", args.output, c.name, date1)
log("Output path: %s", outpath)
logs(outpath)

if c.injectName == nil then c.injectName = true end

local commit = "0000000"
if c.injectCommit then
	commit = exec("git -C %q log --format='%%H' -n 1", args.input)
	local l = #commit
	if l == 41 then
		commit = commit:sub(1, 40)
		l = 40
	end
	assert(l == 40 and not commit:find("[^%l%d]"), "failed to get commit ID")
	commit = commit:sub(1, 7)
	log("Using commit ID %q", commit)
end

local patternsafe = {
	["("] = "%(",
	[")"] = "%)",
	["."] = "%.",
	["%"] = "%%",
	["+"] = "%+",
	["-"] = "%-",
	["*"] = "%*",
	["?"] = "%?",
	["["] = "%[",
	["]"] = "%]",
	["^"] = "%^",
	["$"] = "%$",
	["\0"] = "%z"
}
logv("Preprocessing stripCalls")
for i, func in pairs(c.stripCalls or {}) do
	c.stripCalls[i] = func:gsub(".", patternsafe)
	--logv("%d: %q --> %q", i, func, c.stripCalls[i])
end

local excludestr = ""
for i, exclusion in pairs(c.exclude or {}) do
	excludestr = string.format('%s -not -path "*/%s"', excludestr, exclusion)
end
local liststr = exec("find %q -type f %s", args.input, excludestr)
local list = {}
local listi = 0
logv("Finding files")
for line in liststr:gmatch("([^\n]+)") do
	listi=listi+1
	list[listi] = line
	--logv("%d: %q", listi, line)
end

local fuckwithconf = c.injectName or c.injectCommit or c.injectDate
if c.overrideConf == false then
	log("Not overriding conf")
	fuckwithconf = false
end

logv("Processing files")
for i, path in ipairs(list) do
	local relpath = path:match("^.+/%./(.+)$")
	local outpath = outpath.."/"..relpath
	logv("%d: %q --> %q", i, relpath, outpath)
	
	assert(os.execute(string.format("mkdir -p `dirname %q`", outpath)) == 0, "failed to create directory")
	if relpath:find("%.lua$") then
		local data
		local f = assert(io.open(path, "r"), "failed to open file")
		data = "\n"..f:read("*a").."\n"
		f:close()
		
		if fuckwithconf and relpath == "conf.lua" then
			if c.injectName then
				data = data:gsub('buildName ?= ?[^\n]+', ('buildName=%q--'):format(c.name))
			end
			if c.injectCommit then
				data = data:gsub('commit ?= ?[^\n]+', ('commit=%q--'):format(commit))
			end
			if c.injectDate then
				data = data:gsub('buildAt ?= ?[^\n]+', ('buildAt=%q--'):format(date2))
			end
			if c.overrideConf then
				for k, v in pairs(c.overrideConf) do
					v = type(v) == "string" and ("%q"):format(v) or tostring(v)
					data = data:gsub(('%s ?= ?[^\n;]+'):format(k), ("%s=%s--"):format(k, v))
				end
			end
		end

		if c.honorREMOVEME then
			data = data:gsub("[^\n]*%-%-#REMOVEME\n", "\n")
			data = data:gsub("[^\n]*%-%-#REMOVE_START[^\n]*\n.-[^\n]*%-%-#REMOVE_END", "\n")
		end

		if c.honorINSERTME then
			data = data:gsub("[^\n]*%-%-#INSERTME[ \t]", "")
		end

		if c.stripCalls then
			for _, func in pairs(c.stripCalls) do
				data = data:gsub(("[\n;][ \t]*%s%%(.-%%)[\n;]"):format(func), "\n")
			end
		end
		
		local fn = assert(os.tmpname(), "failed to get temporary file name")
		local f = assert(io.open(fn, "wb+"), "failed to open temporary file")
		f:write(data)
		f:close()
		
		assert(os.execute(string.format("%s -b %q %q", args.compiler, fn, outpath)) == 0, "non-zero code when compiling")
		assert(os.execute(string.format("rm %q", fn)) == 0, "non-zero code when deleting temporary file: "..fn)
	else
		assert(os.execute(string.format("cp %q %q", path, outpath)) == 0, "non-zero code when copying file")
	end
	--]]
end
