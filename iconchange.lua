#!/usr/bin/luajit
local argparse = require "argparse"
local parser = argparse({
	name = "iconchange.lua",
	description = "Change the icon of a Windows executable."
})
parser:argument("exe", "Input executable path.")
parser:argument("ico", "Input icon path.")
parser:argument("output", "Output executable path.")

local args = parser:parse()

local lovepe = require "lib.love-pe"
local f = io.open(args.output, "wb+")
assert(lovepe.patchIcon(io.open(args.exe, "rb"), io.open(args.ico, "rb"), f))
f:flush()
f:close()
