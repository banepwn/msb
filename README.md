# MSB

![MSB logo](logo.png)

MSB is a build tool for [LÖVE](https://love2d.org)/[LÖVR](https://lovr.org) projects. It consists of a preprocessor and a set of packers. When building, the preprocessor will go through your Lua files, patch them according to a configuration file, then compile them. Afterwards, each of the packers will take the assets and patched scripts and produce an archive/executable ready for distribution.

## Supported platforms
Currently, MSB can pack a zip file for Windows and an [AppImage](https://appimage.org/) for Linux. While there is no built-in support for macOS, Android, iOS, or the web, MSB is modular. Create your own packer for these platforms using the existing ones as a reference, and put in your `packers` directory.

## Usage
The intended usage is to modify `override` and `build.sh` for your specific project, then simply run `./build.sh ~/yourgame-repo release`. Look inside each packer's file for information on a specific packer.

## Dependencies
MSB depends on [LuaJIT](https://luajit.org/luajit.html), [`appimagetool`](https://github.com/AppImage/AppImageKit/releases/latest), and [LMMS](https://lmms.io/download#linux) (if using the LMMS renderer functionality). MSB also bundles and makes use of a modified version of [LOVE-PE](https://github.com/RamiLego4Game/love-pe/) by [RamiLego4Game](https://ramilego4game.github.io/).

## License
Copyright © 2020, banepwn

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

**THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.**

This license only includes files belonging to MSB. `lib/love-pe.lua` is protected by a different license included in the file itself.
